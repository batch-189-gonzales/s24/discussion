//Exponent Operator
const firstNum = 8 ** 2;
console.log(firstNum);

const secondNum = Math.pow(8, 2);
console.log(secondNum);

//Template Literals
/*
	Allows to write strings without using the concatenation operator (+)
*/

let name = "Nehemiah";

//Pre-template Literal String
//Using single quote ('')
let message = 'Hello ' + name + '! Welcome to programming.';
console.log('Message without template literals: ' + message);

//Strings Using Template Literal
//Using back tick (``)
message = `Hello ${name}! Welcome to programming.`;
console.log(message);

const anotherMessage = `
${name} attended a math competition.
He won it by solving the problem 8 ** 2 with the solution of ${firstNum}.
`

console.log(anotherMessage);

const interestRate = .1;
let principal = 1000;
console.log(`The interest on your savings is: ${principal * interestRate}`);

//Array Destructuring
/*
	Allows to unpack elements in arrays into disctinct variables.

	Syntax:
		let/const [variableName1, variableName2, ..., variableNameN] = array;
*/

const fullName = ["Juan", "Dela", "Cruz"];
const [firstName, middleName, lastName] = fullName;
console.log(`Hello ${firstName} ${middleName} ${lastName}! It's nice to meet you!`);

//Object Destructuring
/*
	Allows to unpack properties of objects into distinct variables.

	Syntax:
		let/const {propertyName1, propertyName2, ..., propertyNameN} = object;
*/

const person = {
	givenName: "Juana",
	maidenName: "Dela",
	familyName: "Cruz"
};

const {maidenName, givenName, familyName} = person;
console.log(`Hello ${givenName} ${maidenName} ${familyName}.`);

function getFullName({givenName, maidenName, familyName}) {
	console.log(`${givenName} ${maidenName} ${familyName}`)
}
getFullName(person);

//Arrow Functions
/*
	Alternative syntax to traditional functions

	Syntax:
		let/const variableName = (parameter) => {
			statement;
		};
*/

const hello = () => {
	console.log(`Hello`);
};
hello();

function greeting () {
	console.log(`Hello`);
};
greeting();

//Implicit Return Statement
const addition = (x, y) => x + y;
let result = addition(12, 15);
console.log(result);

//Default Function Argument Value
const greet = (name = "User") => {
	return `Good morning, ${name}.`;
};

console.log(greet());
console.log(greet("Grace"));

//Class-based Object Blueprints
/*
	Allows creation/instantiation of objects using classes as blueprints

	Syntax:
		class className {
			constructor (value1, value2, ..., valueN) {
				this.objectProperty1 = value1;
				this.objectProperty2 = value2;
				...
				this.objectPropertyN = valueN
			};
		};
*/

class Car {
	constructor (brand, name, year) {
		this.brand = brand;
		this.name = name;
		this.year = year
	};
};

const myCar = new Car ("Tesla", "Model S", 2021);
console.log(myCar);